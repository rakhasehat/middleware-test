<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TesController extends Controller
{
    public function test1()
    {
        return 'HANYA SUPER ADMIN YANG BISA MENGAKSES INI';
    }

    public function test2()
    {
        return 'HANYA SUPER ADMIN DAN ADMIN YANG BISA MENGAKSES INI';
    }

    public function test3()
    {
        return 'BISA DIAKSES OLEH SEMUA USER';
    }
}
