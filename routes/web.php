<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['role: 2']], function () {
    Route::get('/route-1', 'TesController@test1');
    Route::get('/route-2', 'TesController@test2');
    Route::get('/route-3', 'TesController@test3');
});

Route::group(['middleware' => ['role: 1']], function () {
    Route::get('/route-2', 'TesController@test2');
    Route::get('/route-3', 'TesController@test3');
});

Route::group(['middleware' => ['role']], function () {
    Route::get('/route-3', 'TesController@test3');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/error', 'HomeController@error')->name('error');
