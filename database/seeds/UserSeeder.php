<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Super Admin',
            'email' => 'super@super.com',
            'password' => Hash::make('super123'),
            'role_id' => 2
        ]);

        User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin123'),
            'role_id' => 1
        ]);

        User::create([
            'name' => 'Guest',
            'email' => 'guest@guest.com',
            'password' => Hash::make('guest123')
        ]);
    }
}
