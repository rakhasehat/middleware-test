@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <a href="{{ url('/route-1') }}">Route 1</a>
                    <a href="{{ url('/route-2') }}">Route 2</a>
                    <a href="{{ url('/route-3') }}">Route 3</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
